# Gwarmory

Guild Wars 2 Armory is arming the GW2 community with powered up embeds for items, skills, and more. [NationHive Team](https://www.nationhive.com/jeux/autumns-journey/guide/trophees)

Want something more low level? Want to integrate directly with your React app instead of this high level abstraction? Check out the armory-ui-components library.
Usage

First add the embeds to your html:

<body>
  <div data-armory-embed="skills" data-armory-ids="5507,5508,5510,5515"></div>
</body>

Then add the embed script after them:

<script async src="https://unpkg.com/armory-embeds@^0.x.x/armory-embeds.js"></script>

When the document has fully loaded the embeds will then be loaded.

Note the semvar range. When breaking changes are introduced, this will be incremented by one every time. For example from @^0.x.x to @^1.x.x.
Supplementary Libraries

    armory-embeds-chat-codes - parses an ingame chat code to armory embed markup.

Options
Global Settings

Entirely optional.

Create an object on the document object named GW2A_EMBED_OPTIONS. See below for an example. Make sure the assign this config before declaring the gw2a script.
prop 	type 	required 	description
lang 	string 	no 	The language the embeds will be loaded in. Supported values are: en, fr, de, es, zh, ru
persistToLocalStorage 	boolean 	no 	Turns data persistence on or off. Default to true.
forceCacheClearOnNextRun 	string 	no 	Forces data cache to be cleared. Use wisely. Use a unique key every time you want to clear the cache.

document.GW2A_EMBED_OPTIONS = {
  lang: 'en',
  persistToLocalStorage: true,
  forceCacheClearOnNextRun: '1',
};

Styles

Each embed has a class that you can target, each class follows the pattern .gw2a-{EMBED_NAME}-embed.

.gw2a-character-embed {}
.gw2a-skills-embed {}
.gw2a-items-embed {}
.gw2a-amulets-embed {}
.gw2a-traits-embed {}
.gw2a-specializations-embed {}

Embeds
Skills

<div
  data-armory-embed="skills"
  data-armory-ids="5507,5508,5510,5515">
</div>

<div
  data-armory-embed="skills"
  data-armory-ids="5508,332211,5510,-1"
  data-armory-size="40"
  data-armory-blank-text="This can be replaced with any text!"
>
</div>

